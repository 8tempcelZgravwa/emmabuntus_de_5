#! /bin/bash


# fix_tearing.sh --
#
#   This file permits to fix tearing for the Emmabuntüs Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


########################################################################################################

clear


###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_distribution="Emmabuntus Debian Edition 5"
dir_config=/etc/X11/xorg.conf.d
file_config_intel=20-intel.conf
file_config_amd=20-amd.conf

fix_tearing=false
fix_tearing_apply=false

if [[ $(lspci -G | grep -i graphics | egrep -i "intel|amd") ]] ; then

    if [[ ( -f ${dir_config}/${file_config_intel} || -f ${dir_config}/${file_config_amd} ) ]] ; then
        fix_tearing_apply=true
    else
        fix_tearing=true
    fi
fi

echo "fix_tearing_apply=$fix_tearing_apply"
echo "fix_tearing=$fix_tearing"

source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)

message_demarrage="\n\
$(eval_gettext 'Do you want to fix the picture tearing problem of your Intel/AMD graphics card? ')\n\
\n\
$(eval_gettext 'After applying this hotfix, you need to sign out or restart your computer.')"

message_demarrage_non_applicable="\n\
$(eval_gettext 'This patch is only available for Intel/AMD graphics cards.')"

message_demarrage_appliquer="\n\
$(eval_gettext 'This fix is already applied.                        ')"



    if [[ ${fix_tearing} == true ]] ; then

    export WINDOW_DIALOG='<window title="'$(eval_gettext 'Image tearing fixed')'" icon-name="gtk-dialog-question" resizable="false">
    <vbox spacing="0">

    <text use-markup="true" wrap="false" xalign="0" justify="3">
    <input>echo "'$message_demarrage'" | sed "s%\\\%%g"</input>
    </text>

    <hbox space-expand="false" space-fill="false">
    <button visible="'${BUTTON_CANCEL_VISIBLE}'">
    <input file icon="gtk-no"></input>
    <label>"'$(eval_gettext 'Cancel')'"</label>
    <action>exit:exit</action>
    </button>
    <button can-default="true" has-default="true" use-stock="true" is-focus="true">
    <label>gtk-ok</label>
    <action>exit:OK</action>
    </button>
    </hbox>
    </vbox>

    </window>'

    MENU_DIALOG="$(gtkdialog --center --program=WINDOW_DIALOG)"

    eval ${MENU_DIALOG}
    echo "MENU_DIALOG=${MENU_DIALOG}"


    if  [ ${EXIT} == "OK" ]
    then
        pkexec /usr/bin/fix_tearing_exec.sh
    fi

    else

        if [[ ${fix_tearing_apply} == true ]] ; then

            message_demarrage=${message_demarrage_appliquer}


        else

            message_demarrage=${message_demarrage_non_applicable}
        fi

        export WINDOW_DIALOG='<window title="'${nom_titre_window}'" icon-name="gtk-info" resizable="false">
        <vbox spacing="0">

        <text use-markup="true" wrap="false" xalign="0" justify="3">
        <input>echo "'$message_demarrage'" | sed "s%\\\%%g"</input>
        </text>

        <hbox spacing="10" space-expand="false" space-fill="false">
        <button can-default="true" has-default="true" use-stock="true" is-focus="true">
        <label>gtk-ok</label>
        <action>exit:OK</action>
        </button>
        </hbox>

        </vbox>
        </window>'

        MENU_DIALOG="$(gtkdialog --center --program=WINDOW_DIALOG)"

    fi

exit 0
